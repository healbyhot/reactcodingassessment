import React, { Component } from 'react';
import './App.css';
import Message from "./message/Message";
import messages from "./mockData/MockMessageData"

class App extends Component {
  constructor(props) {
    super(props);
    this.handleMessageClick = this.handleMessageClick.bind(this);
    this.state = {
      // Assuming the message endpoint that this represents returns them sorted chronologically
      messages: messages
    }
  }
  handleMessageClick(event, id){
    // Rest call to modify this message to mark it as read
    // For now we just find the message and update its status
    let msgs = this.state.messages;
    msgs.items.filter( m => m.id === id )[0].is_new = false;
    this.setState( {messages: msgs});
    console.log("Mark Message");
  }
  render() {
    return (
      <div className="App">
        <header><label>Unread Messages: </label>{this.state.messages.items.filter(m => m.is_new === true).length}</header>
        {this.createMessages()}
      </div>
    );
  }
  createMessages() {
    let messageDivs = []
    this.state.messages.items.forEach(message => {
      messageDivs.push(<Message key={message.id} message={message} messageClick={this.handleMessageClick} />)
    });
    return messageDivs;
  }
}

export default App;