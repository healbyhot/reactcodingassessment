import React, { Component } from 'react';
import './Message.css'

class Message extends Component {
    handleClick(event) {
        this.props.messageClick(event, this.props.message.id);
    }
    render() {
        let msg = this.props.message;
        return (
            <div onClick={(event) => { this.props.messageClick(event, msg.id);} } className={msg.is_new === true ? 'newMessage message' : 'message'}>
                <div>From: {msg.from}</div>
                <div className={'toblock'}>To: {this.createSendTos()}</div>
                <div className={msg.is_new === true ? 'unreadMeassage' : ''}>{msg.text}</div>
            </div>
        );
    }
    createSendTos() {
        let divs = []
        this.props.message.to.forEach(to => {
            divs.push(<div className={'toblock'}>{to.number}</div>)
        });
        return divs;
      }
}

export default Message;